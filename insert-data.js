const { MongoClient } = require("mongodb");

// Replace the following with your Atlas connection string

const url = "mongodb+srv://vsv:vivek@1799@cluster0-an8ji.mongodb.net/test?retryWrites=true&w=majority&useNewUrlParser=true&useUnifiedTopology=true";

const client = new MongoClient(url);

 // The database to use
 const dbName = "todoApp";

 async function run() {
    try {
         await client.connect();
         console.log("Connected correctly to server");
         const db = client.db(dbName);

         // Use the collection "todos"
         const col = db.collection("todos");

         // Construct a document
         let todoDocument = {
             item: "make bed"
         }

         // Insert a single document, wait for promise so we can read it back
         const p = await col.insertOne(todoDocument);

         // Find one document
         const myDoc = await col.findOne();

         // Print to the console
         console.log(myDoc);

        } catch (err) {
         console.log(err.stack);
     }

     finally {
        await client.close();
    }
}

run().catch(console.dir);
