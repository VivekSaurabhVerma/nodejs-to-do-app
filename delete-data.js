const { MongoClient } = require("mongodb");
const deleteItems = async () => {
    // connect to your cluster
  const client = await MongoClient.connect("mongodb+srv://vsv:vivek@1799@cluster0-an8ji.mongodb.net/test?retryWrites=true&w=majority&useNewUrlParser=true&useUnifiedTopology=true");
  // specify the DB's name
  const db = client.db('todoApp');
  // execute find query
  await db.collection('todos').deleteOne({item: 'eat sweets'}).then(async function(){
    const items = await db.collection('todos').find({}).toArray();
    console.log(items)
  });

  // close connection
  client.close();

};

deleteItems();
