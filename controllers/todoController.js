const MongoClient = require("mongodb").MongoClient;
var bodyParser = require('body-parser');

const url = "mongodb+srv://vsv:vivek@1799@cluster0-an8ji.mongodb.net/test?retryWrites=true&w=majority&useNewUrlParser=true&useUnifiedTopology=true";

// var data = [{item: 'get milk'}, {item: 'walk dog'}, {item: 'kick some coding ass'}];
var urlencodedParser = bodyParser.urlencoded({extended: false});

module.exports = function(app){
  app.get('/todo', function(req, res){
    // get data from mongodb and pass it to the view
    MongoClient.connect(url, async function(err, database){
      console.log("Connected to database successfully");
      todoApp = await database.db('todoApp');
      var data = await todoApp.collection('todos').find({}).toArray();
      console.log(data);
      console.log('Sending data to view');
      res.render('todo', {todos: data});
    });
  });
  app.post('/todo', urlencodedParser, function(req, res){
    // Get data from view and add it to mongodb
    MongoClient.connect(url, async function(err, database){
      console.log("Connected to database successfully");
      todoApp = await database.db('todoApp');
      console.log('Adding item to database: ');
      console.log(req.body);
      await todoApp.collection('todos').insertOne(req.body);
      data = await todoApp.collection('todos').find({}).toArray();
      res.json({todos:data});
    });
  });
  app.delete('/todo/:item', function(req, res){
    // delete requested item from mongodb
    MongoClient.connect(url, async function(err, database){
      todoApp = await database.db('todoApp');
      await todoApp.collection('todos').deleteOne({item: req.params.item.replace(/\-/g, " ")}).then(async function(){
        data = await todoApp.collection('todos').find({}).toArray();
        res.json({todos: data});
      });
    });
  });
};
