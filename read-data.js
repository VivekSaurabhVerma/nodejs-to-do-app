const { MongoClient } = require("mongodb");
const findItems = async () => {
    // connect to your cluster
  const client = await MongoClient.connect("mongodb+srv://vsv:vivek@1799@cluster0-an8ji.mongodb.net/test?retryWrites=true&w=majority&useNewUrlParser=true&useUnifiedTopology=true");
  console.log("Connected to database successfully");
  // specify the DB's name
  const db = client.db('todoApp');
  // execute find query
  const items = await db.collection('todos').find({}).toArray();
  console.log('Data read successfully');
  console.log(items);
  // close connection
  client.close();

};

findItems();
